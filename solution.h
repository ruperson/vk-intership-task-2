#pragma once 
#include <stdint.h>
#include <vector>

//Algorithm: algo.inria.fr/flajolet/Publications/FlFuGaMe07.pdf
class UniqCounter {
    const static uint32_t M = 1 << 15; 

    //Robert Jenkins' 32 bit integer hash function
    uint32_t hash(int32_t number) {
        number = (number + 0x7ed55d16) + (number << 12u);
        number = (number ^ 0xc761c23c) ^ (number >> 19u);
        number = (number + 0x165667b1) + (number << 5u);
        number = (number + 0xd3a2646c) ^ (number << 9u);
        number = (number + 0xfd7046c5) + (number << 3u);
        number = (number ^ 0xb55a4f09) ^ (number >> 16u);
        return number;
    }
    uint8_t lowest_bit(uint32_t number) {
        for (int i = 0; i < 17; ++i) {
            if ((number >> i) & 1) {
                return i + 1;
            }
        }

        return 18;
    }

    uint8_t registers[M];

  
public:
    UniqCounter() : registers() {}
    void add(int x) {
        uint32_t h = hash(x);
        uint32_t j = 1 + (h & (M - 1));
        auto w = (h >> 15);
        registers[j] = std::max(registers[j], lowest_bit(w));
    }
    int get_uniq_num() const {
        double sum = 0;
        uint32_t v = 0;
        for (uint8_t el : registers) {
            v += (el == 0);
            sum += 1.0 / (1 << el);
        }

        double e = 0.7213 / (1 + 1.079 / M) * M * M / sum;
        double two_32 = 2.0 * (1 << 31);
        if (e <= 5 * M / 2) {
            if (v != 0) {
                return M * std::log(1.0 * M / v);
            }
            else {
                return e;
            }
        }
        else if (e <= 1 / 30 * two_32) {
            return e;
        }
        return -two_32 * std::log(1 - e / two_32);
    }
};
