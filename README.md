
### Описание
1. В файле `solution.h` находится решение тестового задания. Реализован алгоритм HyperLogLog
2. В файле `testing.cpp` находятся тесты, присланные VK Education.

### Требования
cmake, make, С++ compiler
### Как запустить
```bash
mkdir build-release && cd build-release && cmake .. -DCMAKE_BUILD_TYPE=Release && make
./testing
```

